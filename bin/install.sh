#!/bin/env bash


shopt -s inherit_errexit nullglob

error_settings () {
    local error_setting="${1:-on}"
    printf "%s\n" ""
    printf "%s\n" "Setting error settings to $error_setting"
    case "$error_setting" in
        on)
            set -o errexit
            set -o pipefail
            set -o nounset
            set -o errtrace
            trap 'echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow' ERR
            ;;
        off)
            set +o errexit
            set +o pipefail
            set +o nounset
            set +o errtrace
            trap - ERR
            ;;
    esac
}

start_message () {
    printf "%s\n" "Welcome to dano's automated installation script for arch linux!"
    printf "%s\n" "This script will install arch linux with a minimal setup."
    printf "%s\n" "It will install the base system, bootloader, and a few other packages."
    printf "%s\n" "It now downloads some scripts that will setup the system."
}

get_branch_name () {
    local branch_name="main"
    local user_input="input"
    read -r -p "Enter branch name for scripts (Hit return for default(main)): " user_input
    if [ -n "$user_input" ]; then
        branch_name="$user_input"
    fi
    echo "$branch_name"
}

download_scripts () {
    local remote_branch="$1"
    mkdir -p /root/install
    curl https://gitlab.com/olesendan/infrastructure/-/raw/"${remote_branch}"/src/installation/arch-linux/1-drive-setup.sh -o /root/install/1-drive-setup.sh
    chmod +x /root/install/1-drive-setup.sh
    curl https://gitlab.com/olesendan/infrastructure/-/raw/"${remote_branch}"/src/installation/arch-linux/2-base-install.sh -o /root/install/2-base-install.sh
    chmod +x /root/install/2-base-install.sh
    curl https://gitlab.com/olesendan/infrastructure/-/raw/"${remote_branch}"/src/installation/arch-linux/3-system-config.sh -o /root/install/3-system-config.sh
    chmod +x /root/install/3-system-config.sh
    curl https://gitlab.com/olesendan/infrastructure/-/raw/"${remote_branch}"/src/installation/arch-linux/5-post-install.sh -o /root/install/5-post-install.sh
    chmod +x /root/install/5-post-install.sh
}

end_message () {
    printf "%s\n" "Now execute the first script and follow the instructions. Use the following command:"
    printf "%s\n" "/root/install/1-drive-setup.sh"
}

pause ()
{
    printf "%s\n" ""
    local message="Press any key to continue . . ."
    read -r -t 10 -s -n 1 -p "$message" || true
    printf "%s\n" ""
}

main () {
    error_settings on
    start_message
    pause
    local branch_name="main"
    branch_name=$( get_branch_name )
    pause
    download_scripts "$branch_name"
    pause
    end_message
    error_settings off
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "### Script is being executed. ###\n"
    main
else
    printf " This script is being sourced.\n"
fi
