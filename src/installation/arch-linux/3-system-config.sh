#!/bin/env bash


shopt -s inherit_errexit nullglob

error_settings () {
    local error_setting="${1:-on}"
    printf "%s\n" ""
    printf "%s\n" "Setting error settings to $error_setting"
    case "$error_setting" in
        on)
            set -o errexit
            set -o pipefail
            set -o nounset
            set -o errtrace
            trap 'echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow' ERR
            ;;
        off)
            set +o errexit
            set +o pipefail
            set +o nounset
            set +o errtrace
            trap - ERR
            ;;
    esac
}

start_message () {
    printf "%s\n" "This script will setup a basic configuration of a system on Arch linux."
    printf "%s\n" "It should only be executed after the 2-drive-setup.sh script located in root/install"
    printf "%s\n" "You will be asked to enter some information during the installation."
    printf "%s\n" "You can always press Ctrl-C to exit the script."
}

set_do_sys_env_var () {
    local do_sys_env_var="${1:-0}"
    local env_file="${DO_SYS_ENV_FILE}"
    printf "%s\n" "Setting do_sys_env_var to $do_sys_env_var"
    sed -i "s/DO_SYS_SCRIPT_SYSTEM_CONFIG.*/DO_SYS_SCRIPT_SYSTEM_CONFIG=$do_sys_env_var/" "$env_file"
    export DO_SYS_SCRIPT_SYSTEM_CONFIG="$do_sys_env_var"
}

set_host_name () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and does not require host name configuration"
            return 0
            ;;
    esac
    local env_file="${DO_SYS_ENV_FILE}"
    local host_name="host_name"
    read -r -p "Enter host name: " host_name
    sed -i "s/DO_SYS_HOSTNAME.*/DO_SYS_HOSTNAME=$host_name/" "$env_file"
    export DO_SYS_HOSTNAME="$host_name"
    echo "$host_name"
}

configure_timezone () {
    printf "%s\n" "Configuring timezone"
    ln -sf "/usr/share/zoneinfo/$1" /etc/localtime
    hwclock --systohc
    systemctl enable systemd-timesyncd.service
}

change_pacman_settings () {
    grep "Parallel" /etc/pacman.conf
    sed -i "s/^ParallelDownloads = 5/ParallelDownloads = 15/" /etc/pacman.conf
    grep "Parallel" /etc/pacman.conf
}

update_mirrorlist () {
    case ${DO_SYS_SYSTEM_TYPE} in
        container|wsl2)
            pacman -S --noconfirm reflector
            ;;
    esac
    reflector --country Denmark --protocol https --latest 5 --age 12 --sort rate --save /etc/pacman.d/mirrorlist
}

refresh_keyring_and_db () {
    printf "%s\n" "Refreshing archlinux keyring if needed"
    pacman-key --init && pacman-key --populate archlinux
    pacman --noconfirm --needed -Sy archlinux-keyring && pacman --noconfirm -Su
}

set_cpu_vendor () {
    local env_file="${DO_SYS_ENV_FILE}"
    local cpu_vendor="package"
    cpu_vendor=$( lscpu | grep "^Vendor ID" | grep -o -e "Intel" -e "AMD" ) || true
    case "$cpu_vendor" in
        Intel)
            cpu_vendor="intel"
            ;;
        AMD)
            cpu_vendor="amd"
            ;;
        *)
            printf "%s\n" "CPU vendor is not implemented\n"
            exit 1
            ;;
    esac
    sed -i "s/DO_SYS_CPU_VENDOR.*/DO_SYS_CPU_VENDOR=$cpu_vendor/" "$env_file"
    export DO_SYS_CPU_VENDOR="$cpu_vendor"
    echo "$cpu_vendor"
}

set_gpu_vendor () {
    local env_file="${DO_SYS_ENV_FILE}"
    local gpu_vendor="package"
    gpu_vendor=$( lspci | grep -i -e "vga" -e "3d" | grep -o -e "Intel" -e "AMD" -e "NVIDIA" ) || true
    case "$gpu_vendor" in
        Intel)
            gpu_vendor="intel"
            ;;
        AMD)
            gpu_vendor="amd"
            ;;
        NVIDIA)
            gpu_vendor="nvidia"
            ;;
        *)
            printf "%s\n" "GPU vendor is not implemented\n"
            exit 1
            ;;
    esac
    sed -i "s/DO_SYS_GPU_VENDOR.*/DO_SYS_GPU_VENDOR=$gpu_vendor/" "$env_file"
    export DO_SYS_GPU_VENDOR="$gpu_vendor"
    echo "$gpu_vendor"
}

reset_installed_packages () {
    set +o errtrace
    set +o errexit
    set +o pipefail
    printf "%s\n" "Resetting installed packages"
    pacman -D --asdeps $(pacman -Qqe)
    case ${DO_SYS_SYSTEM_TYPE} in
        container|wsl2)
            pacman -D --asexplicit base linux reflector
            ;;
        *)
            pacman -D --asexplicit base base-devel linux linux-firmware linux-zen reflector
            ;;
    esac
    if [[ -n $(pacman -Qtdq) ]]; then
        pacman -Qtdq | pacman --noconfirm -Rns - || true
    else
        printf "%s\n" "No packages to remove"
    fi
    set -o errtrace
    set -o errexit
    set -o pipefail
    return 0
}

install_core_system_packages () {
    local ucode="package"
    local cpu_vendor="${DO_SYS_CPU_VENDOR}"
    printf "%s\n" "Installing core system packages"
    printf "%s\n" "CPU vendor is ${DO_SYS_CPU_VENDOR}"
    case "${cpu_vendor}" in
        intel)
            ucode="intel-ucode"
            ;;
        amd)
            ucode="amd-ucode"
            ;;
        *)
            printf "CPU vendor is not implemented\n"
            exit 1
    esac
    printf "%s\n" "Installing core system packages"
    printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE}"
    local packages=""
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop|desktop|server)
            packages=( "${ucode}" \
                btrfs-progs \
                iwd modemmanager dhcpcd netctl \
                git neofetch \
                tmux neovim \
                sudo openssh \
                man-db man-pages texinfo less \
                terminus-font \
            )
            ;;
        container|wsl2)
            packages=( \
                base base-devel glibc reflector \
                iwd dhcpcd netctl \
                git neofetch \
                tmux neovim \
                sudo openssh \
                man-db man-pages texinfo less \
                terminus-font \
            )
            ;;
        *)
            printf "System type is not implemented\n"
            exit 1
            ;;
    esac
    set +o errexit
    set +o errtrace
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm --needed -S "${packages[@]}"
    set -o errexit
    set -o errtrace
    return 0
}

configure_locale () {
    printf "%s\n" "Configuring locale for programming in Denmark"
    # case ${DO_SYS_SYSTEM_TYPE} in
    #     container|wsl2)
    #         pacman -S --noconfirm glibc
    #         ;;
    # esac
    sed -i "s/^#en_US.UTF-8 UTF-8*/en_US.UTF-8 UTF-8/" /etc/locale.gen
    sed -i "s/^#en_DK.UTF-8 UTF-8*/en_DK.UTF-8 UTF-8/" /etc/locale.gen
    sed -i "s/^#da_DK.UTF-8 UTF-8*/da_DK.UTF-8 UTF-8/" /etc/locale.gen
    locale-gen
    {
        echo "LANG=en_US.UTF-8"
        echo "LANGUAGE=en_US:da_DK"
        echo "LC_CTYPE=en_US.UTF-8"
        echo "LC_NUMERIC=en_DK.UTF-8"
        echo "LC_TIME=en_DK.UTF-8"
        echo "LC_COLLATE=C"
        echo "LC_MONETARY=en_DK.UTF-8"
        echo "LC_MESSAGES=en_US.UTF-8"
        echo "LC_PAPER=da_DK.UTF-8"
        echo "LC_NAME=da_DK.UTF-8"
        echo "LC_ADDRESS=da_DK.UTF-8"
        echo "LC_TELEPHONE=da_DK.UTF-8"
        echo "LC_MEASUREMENT=da_DK.UTF-8"
        echo "LC_IDENTIFICATION=da_DK.UTF-8"
    } > /etc/locale.conf
}

configure_console () {
    printf "%s\n" "Configuring console to font size 16"
    {
        echo "FONT=lat1-16"
    } > /etc/vconsole.conf
}

configure_network_interfaces () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and does not require network configuration"
            return 0
            ;;
    esac
    printf "%s\n" "Configuring network interfaces"
    printf "%s\n" "The following network interfaces are available"
    printf "%s\n" "$(ip link show)"
    printf "%s\n" "Configure ethernet via systemd-networkd"
    local interface_name="interface_name"
    read -r -p "Enter interface name: " interface_name
    {
        echo "[Match]"
        echo "Name=$interface_name"
        echo ""
        echo "[Network]"
        echo "DHCP=yes"
        echo "LinkLocalAddressing=ipv4"
        echo ""

    } > /etc/systemd/network/20-wired.network
    # printf "%s\n" "Configure wifi via systemd-networkd"
    # read -r -p "Enter wifi interface name: " interface_name
    # {
    #     echo "[Match]"
    #     echo "Name=$interface_name"
    #     echo ""
    #     echo "[Network]"
    #     echo "DHCP=yes"
    #     echo "IgnoreCarrierLoss=3s"
    #     echo "LinkLocalAddressing=ipv4"
    # } > /etc/systemd/network/25-wireless.network
}

configure_network_hosts () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and does not require network configuration"
            printf "%s\n" "disabling systemd-resolved, systemd-networkd and dhcpcd"
            systemctl disable systemd-resolved.service
            systemctl disable systemd-networkd.service
            systemctl disable iwd.service
            systemctl disable dhcpcd.service
            return 0
            ;;
    esac
    local hostname="${DO_SYS_HOSTNAME}"
    {
        echo "$hostname"
    } > /etc/hostname
    printf "Setting hosts file"
    {
        echo -e "127.0.0.1\t\tlocalhost"
        echo -e "::1\t\t\tlocalhost"
        echo -e "127.0.1.1\t$hostname"
    } > /etc/hosts
    cat /etc/hosts
    printf "%s\n" "Enabling systemd-resolved and systemd-networkd"
    systemctl enable systemd-resolved.service
    systemctl enable systemd-networkd.service
    systemctl enable iwd.service
    systemctl enable dhcpcd.service
}

setup_ssh () {
    printf "%s\n" "Setting up ssh"
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and can need openssh to be reinstalled"
            pacman -Rns --noconfirm openssh
            pacman -S --noconfirm openssh
            ;;
    esac
    systemctl enable sshd.service
    printf "%s\n" "You can now ssh into the system"
    printf "%s\n" "ssh user@host"
}

update_mkinitcpio_conf () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and does not require mkinitcpio configuration"
            return 0
            ;;
    esac
    local gpu_vendor="${DO_SYS_GPU_VENDOR}"
    printf "%s\n" "Updating mkinitcpio.conf"
    case "${gpu_vendor}" in
        intel)
            NEW_MODULES=(i915 btrfs)
            ;;
        amd)
            NEW_MODULES=(amdgpu btrfs)
            ;;
        *)
            printf "GPU vendor is not implemented\n"
            exit 1
            ;;
    esac
    sed -i "s/^MODULES=.*/MODULES=(${NEW_MODULES[*]})/" /etc/mkinitcpio.conf
    NEW_HOOKS=(base udev autodetect kms modconf keyboard consolefont block btrfs filesystems fsck)
    sed -i "s/^HOOKS=.*/HOOKS=(${NEW_HOOKS[*]})/" /etc/mkinitcpio.conf
    grep "HOOKS" /etc/mkinitcpio.conf
    grep "MODULES" /etc/mkinitcpio.conf
    mkinitcpio -p linux-zen
    mkinitcpio -p linux
}

setup_boot_loader () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and does not require bootloader configuration"
            return 0
            ;;
    esac
    printf "%s\n" "Installing bootloader"
    local cpu_vendor="${DO_SYS_CPU_VENDOR}"
    mkdir -p /boot/loader/entries
    bootctl --esp-path=/boot/ install
    {
        echo "title Arch Linux"
        echo "linux /vmlinuz-linux"
        echo "initrd /${cpu_vendor}-ucode.img"
        echo "initrd /initramfs-linux.img"
        echo 'options root="LABEL=ARCH" rootflags=subvol=rootfs rw'
    } > /boot/loader/entries/arch.conf
    {
        echo "title Arch Linux fallback"
        echo "linux /vmlinuz-linux"
        echo "initrd /${cpu_vendor}-ucode.img"
        echo "initrd /initramfs-linux-fallback.img"
        echo 'options root="LABEL=ARCH" rootflags=subvol=rootfs rw'
    } > /boot/loader/entries/arch-fallback.conf
    {
        echo "title Arch Linux Zen"
        echo "linux /vmlinuz-linux-zen"
        echo "initrd /${cpu_vendor}-ucode.img"
        echo "initrd /initramfs-linux-zen.img"
        echo 'options root="LABEL=ARCH" rootflags=subvol=rootfs rw'
    } > /boot/loader/entries/arch-zen.conf
    {
        echo "title Arch Linux Zen fallback"
        echo "linux /vmlinuz-linux-zen"
        echo "initrd /${cpu_vendor}-ucode.img"
        echo "initrd /initramfs-linux-zen-fallback.img"
        echo 'options root="LABEL=ARCH" rootflags=subvol=rootfs rw'
    } > /boot/loader/entries/arch-zen-fallback.conf
    {
        echo "default arch-zen.conf"
        echo "timeout 4"
        echo "console-mode max"
        echo "editor yes"
    } > /boot/loader/loader.conf
}

change_root_password ()
{
    printf "%s\n" "Changing root password"
    passwd
}

finalize_systemd_config ()
{
    # In wsl 2 systemd tries to run firstboot service
    # this hangs the system
    case "${DO_SYS_SYSTEM_TYPE}" in
        wsl2)
            printf "%s\n" "System type is ${DO_SYS_SYSTEM_TYPE} and requires systemd configuration"
            printf "%s\n" "Setting up systemd for WSL 2"
            systemctl stop systemd-firstboot.service
            systemctl reset-failed
            return 0
            ;;
    esac
}
end_message () {
    printf "%s\n" "The system config script is complete."
    printf "%s\n" "Now you can run the post install script named 5-post-install.sh"
    printf "%s\n" "You should now exit the chroot environment and reboot the system."
    printf "%s\n" "You can exit the chroot environment by executing the following command:"
    printf "%s\n" "exit"
    printf "%s\n" "Then umount the partitions by executing the following commands:"
    printf "%s\n" "umount -R /mnt"
    printf "%s\n" "You can reboot the system by executing the following command:"
    printf "%s\n" "reboot"
}

pause ()
{
    printf "%s\n" ""
    local message="Press any key to continue . . ."
    read -r -t 10 -s -n 1 -p "$message" || true
    printf "%s\n" ""
}

main ()
{
    # setup script environment
    clear
    error_settings on
    start_message
    pause
    # setup do system
    set_do_sys_env_var 0
    set_host_name
    set_cpu_vendor
    # 3.0 system configuration
    # 3.1 Fstab
    # This is created in 2-base-install script because of flow in install scripts
    # 3.2 chroot
    # This is done irl
    # 3.3 time
    configure_timezone "Europe/Copenhagen"
    pause
    #### do-system
    # resetting installed packages
    refresh_keyring_and_db
    change_pacman_settings
    update_mirrorlist
    pause
    reset_installed_packages
    pause
    # installating core system packages
    # as adwiced by arch wiki
    install_core_system_packages
    pause
    # 3.4 localization
    configure_locale
    configure_console
    pause
    # 3.5 network configuration
    configure_network_interfaces
    configure_network_hosts
    # 3.6 initramfs
    update_mkinitcpio_conf
    # 3.7 root password
    change_root_password
    # 3.8 boot loader
    setup_boot_loader
    pause
    # 4.0 reboot
    # is done irl
    # 5.0 post installation
    # wsl 2 container systemd needs to be configured
    finalize_systemd_config
    pause
    #### finalize script
    end_message
    set_do_sys_env_var 1
    error_settings off
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "### Script is being executed. ###\n"
    main
else
    printf " This script is being sourced.\n"
fi
