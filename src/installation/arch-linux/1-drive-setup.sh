#!/bin/bash


shopt -s inherit_errexit nullglob

error_settings () {
    local error_setting="${1:-on}"
    printf "%s\n" ""
    printf "%s\n" "Setting error settings to $error_setting"
    case "$error_setting" in
        on)
            set -o errexit
            set -o pipefail
            set -o nounset
            set -o errtrace
            trap 'echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow' ERR
            ;;
        off)
            set +o errexit
            set +o pipefail
            set +o nounset
            set +o errtrace
            trap - ERR
            ;;
    esac
}

start_message () {
    printf "%s\n" ""
    printf "%s\n" "This script will setup drives for Arch Linux installation."
    printf "%s\n" "It will create a boot drive and a system drive."
    printf "%s\n" "The boot drive will be formatted as fat32."
    printf "%s\n" "The system drive will be formatted as btrfs."
    printf "%s\n" ""
}

setup_do_sys_env_var () {
    local env_file="${1:-/root/install/do-system.sh}"
    {
        echo "DO_SYS_SCRIPT_DRIVE_SETUP=0"
        echo "DO_SYS_SCRIPT_BASE_INSTALL=0"
        echo "DO_SYS_SCRIPT_SYSTEM_CONFIG=0"
        echo "DO_SYS_SCRIPT_POST_INSTALL=0"
        echo "DO_SYS_ENV_FILE=\"\""
        echo "DO_SYS_ROOTFS_DEVICE=\"\""
        echo "DO_SYS_SYSTEM_TYPE=\"\""
        echo "DO_SYS_HOSTNAME=\"\""
        echo "DO_SYS_CPU_VENDOR=\"\""
        echo "DO_SYS_GPU_VENDOR=\"\""
    } > "$env_file"
    export DO_SYS_ENV_FILE="$env_file"
    echo "$env_file"
}

setup_ssh_install ()
{
    echo root:1234 | chpasswd
}

reset_devices () {
    printf "%s\n" "Resetting devices."
    swapoff -a
    umount -R /mnt
}

devices_on_system ()
{
    case "${1:-less}" in
        full)
            printf "%s\n" "devices in lsblk\n"
            printf "%s\n" "$( lsblk )"
            ;;
        *)
            printf "%s\n" "$( lsblk -o NAME,PARTLABEL,FSTYPE )"
            ;;
    esac
}

set_device_to_format () {
    local dev="dev"
    local device="device"
    local env_file="${DO_SYS_ENV_FILE}"
    read -r -p "Enter device to format: " dev
    device="/dev/$dev"
    sed -i "s/DO_SYS_ROOTFS_DEVICE=.*/DO_SYS_ROOTFS_DEVICE=\"$device\"/" "$env_file"
    export DO_SYS_ROOTFS_DEVICE="$device"
    echo "$device"
}

format_device () {
    local device="${DO_SYS_ROOTFS_DEVICE}"
    printf "%s\n" "selected device"
    echo "$device"
    parted --script --fix --align optimal "$device" mklabel gpt \
        mkpart '"EFI system partition"' fat32 1MiB 1001MiB \
        mkpart '"Linux filesystem"' btrfs 1001MiB 100% \
        set 1 esp on
    devices_on_system
}

create_filesystem () {
    local dev="${DO_SYS_ROOTFS_DEVICE}"
    printf "%s\n" "Creating file system"
    local dev_boot="$dev"1
    printf "%s\n" "$dev_boot"
    mkfs.fat -F 32 -n BOOT "$dev_boot"
    local dev_sys="$dev"2
    mkfs.btrfs -L ARCH -n 32k -f "$dev_sys"
    printf "%s\n" "filesystems on devices"
    devices_on_system
}

subvolumes ()
{
    local subs=( root home swap var_log var_tmp var_abs var_cache var_spool var_lib_machines var_lib_docker var_lib_mysql var_lib_sqlite var_lib_mssql var_opt_mssql var_lib_sql srv tmp snapshots )
    echo "${subs[@]}"
}

setup_btrfs_filesystem ()
{
    printf "%s\n" "Creating relevant btrfs subvolumes."
    local dev_sys="${DO_SYS_ROOTFS_DEVICE}"2
    local subvol="/mnt/rootfs"
    mount "$dev_sys" /mnt
    printf "%s\n" "$( ls /mnt)"
    btrfs su cr "${subvol}"

    local subs=( $( subvolumes ) )
    for sub in "${subs[@]}"; do
        local subvol="/mnt/$sub"
        btrfs su cr "$subvol"
    done
    printf "%s\n" "$( ls /mnt)"
    #btrfs su list -p /mnt
    umount /mnt
}

mount_btrfs_subvolumes ()
{
    local dev_sys="$1"2
    printf "%s\n" "Mounting btrfs subvolumes."
    local subvol="rootfs"
    local mnt_point="/mnt"
    printf  "%s\n" "Mounting $subvol at $mnt_point on $dev_sys"
    mount -o defaults,noatime,compress-force=zstd:2,commit=120,autodefrag,discard=async,ssd,space_cache=v2,subvol="$subvol" "$dev_sys" "$mnt_point"
    local subs=( $( subvolumes ) )
    for sub in "${subs[@]}"; do
        subvol="$sub"
        mnt_point="/mnt/${sub//_//}"
        printf "%s\n" "Mounting $subvol at $mnt_point on $dev_sys"
        mount --mkdir -o defaults,noatime,compress-force=zstd:2,commit=120,autodefrag,discard=async,ssd,space_cache=v2,subvol="$subvol" "$dev_sys" "$mnt_point"
    done
}

mount_boot_partition () {
    local dev_boot="$1"1
    mnt_point="/mnt/boot"
    printf "%s\n" "Mounting $dev_boot at $mnt_point"
    mkdir -p "$mnt_point"
    # from https://bbs.archlinux.org/viewtopic.php?id=287790
    mount "$dev_boot" "$mnt_point"
}

change_permissions_on_specific_mounts ()
{
    local subs=( boot )
    for sub in "${subs[@]}"; do
        local mnt_point="/mnt/${sub}"
        printf "%s\n" "Changing permissions on $mnt_point"
        chmod 700 "$mnt_point"
    done
    local subs=( root )
    for sub in "${subs[@]}"; do
        local mnt_point="/mnt/${sub//_//}"
        printf "%s\n" "Changing permissions on $mnt_point"
        chmod 750 "$mnt_point"
    done
    local subs=( var_tmp )
    for sub in "${subs[@]}"; do
        local mnt_point="/mnt/${sub//_//}"
        printf "%s\n" "Changing permissions on $mnt_point"
        chmod 777 "$mnt_point"
    done
}

create_swap_file () {
    btrfs filesystem mkswap --size 4g --uuid clear /mnt/swap/swapfile
    swapon /mnt/swap/swapfile
    swapon -a
    printf "%s\n" "$( lsblk )"
}


set_do_sys_env_var () {
    local env_var="${1:-0}"
    local env_file="${DO_SYS_ENV_FILE}"
    sed -i "s/DO_SYS_SCRIPT_DRIVE_SETUP=.*/DO_SYS_SCRIPT_DRIVE_SETUP=$env_var/" "$env_file"
    export DO_SYS_SCRIPT_DRIVE_SETUP="$env_var"
}

end_message ()
{
    printf "%s\n" ""
    printf "%s\n" "Drive setup complete."
    printf "%s\n" "You can now run the next script."
    printf "%s\n" "Next script is 2-base-install.sh"
    printf "%s\n" "Run it with the following command:"
    printf "%s\n" "/root/install/2-base-install.sh"
    printf "%s\n" ""
}

pause () {
    printf "%s\n" ""
    local message="Press any key to continue . . ."
    read -r -t 10 -s -n 1 -p "$message" || true
    printf "%s\n" ""
}
main () {
    ### Setup script
    local env_file="/root/install/do-system.sh"
    clear
    error_settings on
    start_message
    pause
    # 1 pre-installation
    #### the following steps are done irl
    # 1.1 Acquire install image
    # 1.2 Verify signature
    # 1.3 prepare installation medium
    # 1.4 Boot the live environment
    # 1.5 set the console keyboard layout and font
    # 1.6 verify the boot mode
    # 1.7 connect to the internet
    # 1.8 update the system clock
    ####

    # setup do system
    setup_do_sys_env_var "${env_file}"
    set_do_sys_env_var
    # setup_ssh_install
    error_settings off
    reset_devices
    error_settings on
    # 1.9 partition the disks
    devices_on_system full
    set_device_to_format
    format_device
    pause
    # 1.10 format the partitions
    create_filesystem
    setup_btrfs_filesystem
    pause
    # 1.11 mount the file systems
    mount_btrfs_subvolumes
    mount_boot_partition
    pause
    change_permissions_on_specific_mounts
    create_swap_file
    pause
    ### finalize script
    set_do_sys_env_var 1
    end_message
    error_settings off
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "### Script is being executed. ###\n"
    main
else
    printf " This script is being sourced.\n"
fi
