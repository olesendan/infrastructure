#!/bin/env bash

shopt -s inherit_errexit nullglob

error_settings () {
    local error_setting="${1:-on}"
    printf "%s\n" ""
    printf "%s\n" "Setting error settings to $error_setting"
    case "$error_setting" in
        on)
            set -o errexit
            set -o pipefail
            set -o nounset
            set -o errtrace
            trap 'echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow' ERR
            ;;
        off)
            set +o errexit
            set +o pipefail
            set +o nounset
            set +o errtrace
            trap - ERR
            ;;
    esac
}

start_message () {
    printf "%s\n" "This script will performe a post installation setup on a arch system."
    printf "%s\n" "It should only be executed after the 3-system-config.sh script located in root/install"
    printf "%s\n" "You will be asked to enter some information during the installation."
    printf "%s\n" "You can always press Ctrl-C to exit the script."
}

set_do_sys_env_var () {
    local env_var="${1:-0}"
    local env_file="${DO_SYS_ENV_FILE}"
    printf "%s\n" "Setting do_sys_env_var to $env_var"
    sed -i "s/DO_SYS_SCRIPT_POST_INSTALLATION.*/DO_SYS_SCRIPT_POST_INSTALLATION=$env_var/" "$env_file"
    export DO_SYS_SCRIPT_POST_INSTALLATION="$env_var"
}

set_system_type() {
    local env_file="${DO_SYS_ENV_FILE}"
    local system_type=0
    local system_types=( laptop desktop server container wsl2 )
    if [ -n "${WSL_DISTRO_NAME}" ]; then
        system_type=1
    fi
    echo "$system_type"
    if [ $system_type = 0 ]; then
        local inside_container=0
        inside_container=$( grep -a 'container' -c /proc/1/environ || true )
        if [ "$inside_container" -gt 0 ]; then
            system_type=2
        else
            system_type=3
        fi
    fi
    case "$system_type" in
        1)
            printf "%s\n" "system_type is set to wsl2"
            system_type="wsl2"
            ;;
        2)
            printf "%s\n" "system_type is set to container"
            system_type="container"
            ;;
        3)
            printf "%s\n" "system_type is set based on input"
            read -r -p "Enter system type (${system_types[*]}): " system_type
            ;;
        *)
            printf "%s\n" "system_type is not set"
            return 0
            ;;
    esac
    if [[ ! " ${system_types[*]} " =~ ${system_type} ]]; then
        printf "%s\n" "System type is not in the list"
        exit 1
    fi
    sed -i "s/DO_SYS_SYSTEM_TYPE.*/DO_SYS_SYSTEM_TYPE=$system_type/" "$env_file"
    export DO_SYS_SYSTEM_TYPE="$system_type"
    echo "$system_type"
}

add_super_user () {
    # Add user to the wheel group
    # Gets username as input
    # https://wiki.archlinux.org/index.php/Users_and_groups
    local user_name=""
    local user_exists=""
    read -r -p "Enter user name: " user_name
    user_exists=$(grep -c -o "${user_name}" /etc/passwd || true)
    if [[ "${user_exists}" == 1 ]]; then
        printf "%s\n" "User $user_name already exists"
        return 0
    else
        printf "%s\n" "Adding user: $user_name"
        useradd -m -G wheel -s /bin/bash "$user_name"
        echo "$user_name:1234" | chpasswd
    fi
    printf "%s\n" "You can change the password by executing the following command:"
    printf "%s\n" "passwd $user_name"
    printf "%s\n" "You can add more super users by executing the following command:"
    printf "%s\n" "add_super_user"
}

setup_wheel_group () {
    # Enable wheel group in sudoers
    printf "%s\n" "Adding super user to sudoers"
    sed --in-place "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/" /etc/sudoers
    grep "wheel" /etc/sudoers
}

config_ssh_login () {
    printf "%s\n" "Configuring ssh login"
    # https://wiki.archlinux.org/title/OpenSSH#Limit_root_login
    echo "PermitRootLogin no" > /etc/ssh/sshd_config.d/20-deny-root-login.conf
    systemctl restart sshd.service
}

setup_systemd_and_journal () {
    printf "%s\n" "Setting up systemd and journal"
    printf "%s\n" "show systemd status"
    # case "${DO_SYS_SYSTEM_TYPE}" in
    #     container|wsl2)
    #         systemctl stop systemd.firstboot.service
    #         systemctl disable networkd.service
    #         systemctl disable networkd.socket
    #         systemctl disable systemd-resolved.service
    #         systemctl disable systemd-resolved.socket
    #         systemctl disable systemd-networkd-wait-online.service
    #         systemctl restart console-getty.service
    #         systemctl enable --now dbus
    #         systemctl start sshd
    #         ;;
    # esac
    systemctl status
    systemctl --failed
    systemctl list-jobs
    systemctl list-unit-files
    printf "%s\n" "Setup journal"
    sed -i 's/#Storage=auto/Storage=persistent/' /etc/systemd/journald.conf
    printf "%s\n" "show journal status"
    journalctl --list-boots
    journalctl --disk-usage
    journalctl -ex -b -p 4..0

}

add_packages () {
    printf "%s\n" "Installing package"
    local packages=( pacman-contrib turbostat zram-generator btop coreutils findutils docker docker-compose )
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm -Syu
    pacman --noconfirm --needed -S "${packages[@]}"
    case "${DO_SYS_SYSTEM_TYPE}" in
        server)
            local server_packages=( dosfstools ntfs-3g )
            printf "%s " "Packages to be installed ${server_packages[*]}"; printf "\n"
            pacman --noconfirm --needed -S "${server_packages[@]}"
            ;;
    esac
}

setup_paccache () {
    mkdir -p /etc/pacman.d/hooks
    printf "%s\n" "setup paccache"
    {
        echo "[Trigger]"
        echo "Operation = Upgrade"
        echo "Operation = Install"
        echo "Operation = Remove"
        echo "Type = Package"
        echo "Target = *"
        echo ""
        echo "[Action]"
        echo "Description = Cleaning pacman cache..."
        echo "When = PostTransaction"
        echo "Exec = /usr/bin/paccache -rvk2"
        } > /etc/pacman.d/hooks/clean_package_cache.hook
}

disable_tty_clearance () {
    printf "%s\n" "Disabling tty clearance on login"
    mkdir -p /etc/systemd/system/getty@tty1.service.d
    {
        echo "[Service]"
        echo "TTYVTDisallocate=no"
    } > /etc/systemd/system/getty@tty1.service.d/noclear.conf
}

# Obsolete with wayland
# install_display_driver () {
#     printf "%s\n" "Installing display driver"
#     case "${DO_SYS_GPU_VENDOR}" in
#         intel)
#             local packages=( xf86-video-intel )
#             ;;
#         amd)
#             local packages=( xf86-video-amdgpu )
#             ;;
#         nvidia)
#             local packages=( nvidia nvidia-utils )
#             ;;
#         nouveau)
#             local packages=( xf86-video-nouveau )
#             ;;
#     esac
#     printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
#     pacman --noconfirm --needed -S "${packages[@]}"
# }

setup_xdg_user_dirs () {
    printf "%s\n" "Setting up xdg user directories in root bash_profile."
    if [ ! -f /root/.bash_profile ]; then
        touch /root/.bash_profile
        {
            echo "# xdg user directories"
            echo "export XDG_CONFIG_HOME=/root/.config"
            echo "export XDG_CACHE_HOME=/root/.cache"
            echo "export XDG_DATA_HOME=/root/.local/share" 
        } > /root/.bash_profile
    fi
    # if [ ! -f /home/dano/.bash_profile ]; then
    #     touch /home/dano/.bash_profile
    #     {
    #         echo "# xdg user directories"
    #         echo "export XDG_CONFIG_HOME=/home/dano/.config"
    #         echo "export XDG_CACHE_HOME=/home/dano/.cache"
    #         echo "export XDG_DATA_HOME=/home/dano/.local/share" 
    #     } > /home/dano/.bash_profile
    # fi
}

set_acpi_events () {
    printf "%s\n" "Setting up acpi events"
    # https://wiki.archlinux.org/index.php/
    # https://wiki.archlinux.org/index.php/Power_management#ACPI_events
    # acpi events are handled by systemd
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop|desktop)
            mkdir -p /etc/systemd/logind.conf.d
            {
                echo "[Login]"
                echo "HandlePowerKey=poweroff"
                echo "HandleLidSwitch=ignore"
                echo "HandleLidSwitchExternalPower=ignore"
                echo "HandleLidSwitchDocked=ignore"
            } > /etc/systemd/logind.conf.d/acpi_events.conf
            # local packages=( acpid )
            # printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
            # pacman --noconfirm --needed -S "${packages[@]}"
            # systemctl enable --now acpid
            ;;
        server|container|wsl2)
            mkdir -p /etc/systemd/logind.conf.d
            {
                echo "[Login]"
                echo "HandlePowerKey=ignore"
                echo "HandleLidSwitch=ignore"
                echo "HandleLidSwitchExternalPower=ignore"
                echo "HandleLidSwitchDocked=ignore"
            } > /etc/systemd/logind.conf.d/acpi_events.conf
            ;;
    esac
}

setup_cpu_frequency_scaling () {
    printf "%s\n" "Setting up cpu frequency scaling"
    # https://wiki.archlinux.org/index.php/CPU_frequency_scaling
    # https://wiki.archlinux.org/index.php/Power_management#ACPI_events
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop|desktop|server)
            local packages=( cpupower )
            printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
            pacman --noconfirm --needed -S "${packages[@]}"
            systemctl enable --now cpupower.service
            printf "%s\n" "Scaling driver needs to be set up."
            ;;
        container|wsl2)
            printf "%s\n" "Cpu frequency scaling is not set up for $DO_SYS_SYSTEM_TYPE"
            ;;
    esac
}

setup_laptop () {
    printf "%s\n" "Setting up laptop"
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop)
            printf "%s\n" "Chech the arch wiki for more information on setting up a laptop"
            printf "%s\n" "https://wiki.archlinux.org/index.php/Laptop"
            printf "%s\n" "Candidates for laptop setup:"
            printf "%s\n" "Backlight"
            printf "%s\n" "Touchpad"
            printf "%s\n" "Webcam"
            printf "%s\n" "Bluetooth"
            printf "%s\n" "Fingerprint reader"
            printf "%s\n" "Hybrid graphics"
            printf "%s\n" "Audio mute LED"
            printf "%s\n" "See keycodes in wayland wev"
            return 0
            ;;
        desktop|server|container|wsl2)
            return 0
            ;;
    esac
}

setup_suspend () {
    printf "%s\n" "Setting up suspend."
    # https://wiki.archlinux.org/index.php/CPU_frequency_scaling
    # https://wiki.archlinux.org/index.php/Power_management#ACPI_events
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop)
            local packages=( tlp powertop )
            ;;
        desktop|server)
            local packages=( powertop )
            ;;
        container|wsl2)
            printf "%s\n" "Suspend is not relevant for ${DO_SYS_SYSTEM_TYPE}"
            return 0
            ;;
    esac
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm --needed -S "${packages[@]}"
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop)
            systemctl enable --now tlp
            systemctl enable --now tlp-sleep
            systemctl enable --now tlp-sleep.service
            ;;
        desktop|server)
            printf "%s\n" "TLP is not set up for ${DO_SYS_SYSTEM_TYPE}"
            ;;
    esac
    case "${DO_SYS_SYSTEM_TYPE}" in
        laptop|desktop|server)
            powertop --auto-tune
            powertop --calibrate
            powertop --html=powertop.html
            ;;
    esac
}

setup_sound () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2|server)
            printf "%s\n" "Sound is not set up for $DO_SYS_SYSTEM_TYPE"
            return 0
            ;;
    esac
    printf "%s\n" "Setting up sound"
    pacman --noconfirm --needed -S alsa-utils
    systemctl enable --now alsa-restore.service
    amixer sset Master unmute
    amixer sset Speaker unmute
    amixer sset Headphone unmute
}

setup_firewall () {
    printf "%s\n" "Setting up firewall"
    # https://wiki.archlinux.org/index.php/Firewall
    case ${DO_SYS_SYSTEM_TYPE} in
        server|container|wsl2)
            printf "%s\n" "Firewall is not set up for $DO_SYS_SYSTEM_TYPE"
            return 0
            ;;
    esac
    local packages=( ufw )
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm --needed -S "${packages[@]}"
    systemctl enable --now ufw
    # ufw default deny
    ufw enable
    ufw status
    ufw status verbose
    ufw show raw
    ufw app list
}

setup_network_shares () {
    printf "%s\n" "Setting up network shares"
}

setup_benchmarking () {
    printf "%s\n" "Setting up benchmarking"
    # https://wiki.archlinux.org/index.php/Benchmarking
    local packages=( time )
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm --needed -S "${packages[@]}"
}

setup_zram () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "Zram is not set up for ${DO_SYS_SYSTEM_TYPE}"
            return 0
            ;;
    esac
    printf "%s\n" "Setting up zram"
    # https://wiki.archlinux.org/title/Zram
    # https://wiki.archlinux.org/title/Improving_performance#Zram_or_zswap
    {
        echo "[zram0]"
        echo "zram-size = ram * 2"
        echo "compression-algorithm = zstd"
        echo "swap-priority = 100"
        echo "fs-type = swap"
    } > /etc/systemd/zram-generator.conf
    systemctl daemon-reload
    # systemctl start --now zram0.service
    zramctl
    zramctl zram0
    {
        echo "vm.swappiness = 180"
        echo "vm.watermark_boost_factor = 0"
        echo "vm.watermark_scale_factor = 125"
        echo "vm.page-cluster = 0"
    } > /etc/sysctl.d/99-vm-zram-parameters.cong
}

setup_fs_trim () {
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "Fs trim is not set up for $DO_SYS_SYSTEM_TYPE"
            return 0
            ;;
    esac
    systemctl start --now fstrim.timer
}

setup_printing () {
    printf "%s\n" "Setting up printing"
    # https://wiki.archlinux.org/index.php/CUPS
    local packages=( cups )
    case "${DO_SYS_SYSTEM_TYPE}" in
        container|wsl2)
            printf "%s\n" "Printing is not set up for $DO_SYS_SYSTEM_TYPE"
            return 0
            ;;
        desktop|laptop)
            packages+=( cups-pdf )
            ;;
        server)
            ;;
    esac
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm --needed -S "${packages[@]}"
    systemctl enable --now cups.socket
}

setup_font () {
    printf "%s\n" "Setting up fonts"
    # https://wiki.archlinux.org/index.php/Font_configuration
    # https://wiki.archlinux.org/index.php/Fonts
    case "${DO_SYS_SYSTEM_TYPE}" in
        container)
            printf "%s\n" "Fonts are not set up for $DO_SYS_SYSTEM_TYPE"
            return 0
            ;;
    esac
    local packages=( fontconfig ttf-jetbrains-mono-nerd ttf-firacode-nerd )
    printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    pacman --noconfirm -Syu
    pacman --noconfirm --needed -S "${packages[@]}"
    fc-cache -f
}

setup_extended_completion () {
    printf "%s\n" "Setting up extended completion"
    # https://wiki.archlinux.org/index.php/Bash#Tab_completion
    # https://wiki.archlinux.org/index.php/Zsh#Tab_completion
    # local packages=( bash-completion )
    # printf "%s " "Packages to be installed ${packages[*]}"; printf "\n"
    # pacman --noconfirm --needed -S "${packages[@]}"
    {
        echo "compopt -o bashdefault rm mkdir mv cp ls cat"
    } >> /root/.bash_completion
    {
        echo "# settings for Readline, which is used by Bash"
        echo "set editing-mode vi"
        echo "set show-mode-in-prompt on"
        echo "set completion-ignore-case on"
        echo "set show-all-if-ambiguous on"
        echo "set colored-stats On"
        echo "set visible-stats On"
        echo "set mark-symlinked-directories On"
        echo "set colored-completion-prefix On"
        echo "set menu-complete-display-prefix On"
    } >> /root/.inputrc
}

setup_bash_aliases () {
    printf "%s\n" "Setting up bash aliases"
    # https://wiki.archlinux.org/index.php/Bash#Aliases
    {
        echo "alias ls='ls --color=auto'"
        echo "alias ll='ls -l'"
        echo "alias la='ls -la'"
        echo "alias l='ls -CF'"
        echo "alias grep='grep --color=auto'"
        echo "alias fgrep='fgrep --color=auto'"
        echo "alias egrep='egrep --color=auto'"
    } >> /root/.bash_aliases
}

# 12.3 Alternative shells
# Using Bash as default shell

# 12.4 Bash Additions
setup_bash_additions () {
    printf "%s\n" "Setting up bash additions"
    # https://wiki.archlinux.org/index.php/Bash#Additions
        mkdir -p /root/.local/share/bash/
    {
        echo "# settings for Readline, which is used by Bash"
        echo "set editing-mode vi"
        echo "set show-mode-in-prompt on"
        echo "set completion-ignore-case on"
        echo "set show-all-if-ambiguous on"
        echo "set colored-stats On"
        echo "set visible-stats On"
        echo "set mark-symlinked-directories On"
        echo "set colored-completion-prefix On"
        echo "set menu-complete-display-prefix On"
    } >> /root/.inputrc
    {
        printf "%s\n" "bind '"\\e[A": history-search-backward'"
        printf "%s\n" "bind '"\\e[B": history-search-forward'"
        echo "export HISTFILE=/root/.local/share/bash/bash_history"
        echo "shopt -s histappend"
        echo "shopt -s checkwinsize"
        echo "export HISTCONTROL=erasedups:ignoreboth:ignorespace"
        echo "export HISTSIZE=10000"
        echo "export HISTFILESIZE=20000"
        echo "shopt -s autocd"
    } >> /root/.bashrc
    {
        echo "clear"
        echo "reset"
    } >> /root/.bash_logout
}

end_message () {
    printf "%s\n" "The post installation setup script is done.."
    printf "%s\n" "You should now reboot the system."
    printf "%s\n" "You can reboot the system by executing the following command:"
    printf "%s\n" "reboot"
    printf "%s\n" "There still missing a password store and a firewall."
}

pause ()
{
    printf "%s\n" ""
    local message="Press any key to continue . . ."
    read -r -t 10 -s -n 1 -p "$message" || true
    printf "%s\n" ""
}

main ()
{
    # setup environment and script settings
    clear
    error_settings on
    start_message
    set_do_sys_env_var 0
    set_system_type
    pause
    # 1 System administation
    # 1.1 Users and Groups
    add_super_user
    # 1.2 Security
    setup_wheel_group
    config_ssh_login
    # 1.3 Service management
    setup_systemd_and_journal
    pause
    # 2 Package management
    # 2.1 pacman
    add_packages "$DO_SYS_SYSTEM_TYPE"
    setup_paccache
    pause
    # 2.2 Repositories
    # 2.3 Mirrors
    # Is handled by reflector in the 3-system-config.sh script
    # 2.4 Arch build system
    # 2.5 Arch User Repository

    # 3 Booting
    # 3.1 Hardware auto-recognition
    # Is handled in the 3-system-config.sh script
    # 3.2 Microcode
    # Is set in the 3-system-config.sh script
    # 3.3 Retaining boot messages
    disable_tty_clearance
    pause
    # 4 Graphical user interface
    # 4.1 Display server 
    # This is handled elsewhere
    # 4.2 Display drivers
    # is handled in the 3-system-config.sh script
    # because wayland is used
    # install_display_driver
    # 4.6 User Directories
    setup_xdg_user_dirs "$DO_SYS_SYSTEM_TYPE"
    pause
    # 5.0 Power management
    # 5.1 ACPI events
    set_acpi_events
    pause
    # 5.2 CPU frequency scaling
    setup_cpu_frequency_scaling
    # 5.3 Laptops
    setup_laptop
    # 5.4 Suspend and hibernate
    setup_suspend
    # 6.0 Multimedia
    # 6.1 Sound
    # extra sound server should be installed in setup of laptop/desktop
    setup_sound "$DO_SYS_SYSTEM_TYPE"
    # 7 Networking
    # 7.1 DNS security
    # This can be relevant for the dns server
    # setup_dns_security
    # 7.2 Firewall
    setup_firewall

    # 7.3 Network shares
    # setup_network_shares

    # 8 Input devices
    # 8.1 Keyboard layouts
    # Keyboard layout is handled in the 3-system-config.sh script

    # 8.2 Mouse buttons
    # Not setup

    # 8.3 Laptop touchpad
    # Setup in the 5.3 Laptops

    # 8.4 TrackPoints
    # Not implemented yet

    # 9 Optimization
    # 9.1 benchmarking
    setup_benchmarking
    # 9.2 Improving performance
    setup_zram
    pause
    # 9.3 Solid state drives
    setup_fs_trim
    pause
    # 10 System services
    # 10.1 File index and search
    # not implemented yet
    # 10.2 Local mail delivery
    # not implemented yet
    # 10.3 Printing
    setup_printing
    pause
    # 11 Appearance
    # 11.1 Fonts
    setup_font
    pause
    # 11.2 GTK and Qt themes
    # Not implemented yet

    # 12 Console improvements
    # 12.1 Tab-completion enhancements
    setup_extended_completion
    # 12.2 Aliases
    setup_bash_aliases
    # 12.3 Alternative shells
    # Using Bash as default shell
    # 12.4 Bash Additions
    setup_bash_additions
    pause
    end_message
    set_do_sys_env_var 1
    error_settings off
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "### Script is being executed. ###\n"
    main
else
    printf " This script is being sourced.\n"
fi
