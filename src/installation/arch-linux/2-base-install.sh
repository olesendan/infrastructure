#!/bin/env bash

shopt -s inherit_errexit nullglob

error_settings () {
    local error_setting="${1:-on}"
    printf "%s\n" ""
    printf "%s\n" "Setting error settings to $error_setting"
    case "$error_setting" in
        on)
            set -o errexit
            set -o pipefail
            set -o nounset
            set -o errtrace
            trap 'echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow' ERR
            ;;
        off)
            set +o errexit
            set +o pipefail
            set +o nounset
            set +o errtrace
            trap - ERR
            ;;
    esac
}

start_message () {
    printf "%s\n" ""
    printf "%s\n" "This script will install a basic Arch linux install."
    printf "%s\n" "It should only be executed after the drive-setup.sh script located in root"
    printf "%s\n" "You will be asked to enter some information during the installation."
    printf "%s\n" "You can always press Ctrl-C to exit the script."
    printf "%s\n" ""
}

set_do_sys_env_var () {
    printf "%s\n" "Setting DO_SYS environment variable to true"
    local env_var="${1:-0}"
    local env_file="${DO_SYS_ENV_FILE}"
    sed -i "s/^DO_SYS_SCRIPT_BASE_INSTALL.*/DO_SYS_SCRIPT_BASE_INSTALL=${env_var}/" "$env_file"
    export DO_SYS_SCRIPT_BASE_INSTALL="$env_var"
    }

update_mirrorlist ()
{
    reflector --country Denmark --protocol https --latest 5 --age 24 --sort rate --save /etc/pacman.d/mirrorlist
    cat /etc/pacman.d/mirrorlist
}

reset_base_install ()
{
    set +o errtrace
    set +o errexit
    printf "%s\n" ""
    printf "%s\n" "Removing old core installation"
    local mount_point="$1"
    rm -rf "$mount_point"
    set -o errtrace
    set -o errexit
    return 0
}

change_pacman_settings () {
    grep "Parallel" /etc/pacman.conf
    sed -i "s/^#ParallelDownloads = 5/ParallelDownloads = 5/" /etc/pacman.conf
}

install_base_packages ()
{
    set +o errtrace
    set +o errexit
    pacman --noconfirm --needed -Sy archlinux-keyring
    pacstrap -K /mnt base base-devel linux linux-firmware linux-zen reflector
    set -o errtrace
    set -o errexit
    # pactrap fails to create mkinicpio,
    # so we need to create it manually when using arch-chroot
    # therefore force a successful exit
    return 0
}

generate_fstab ()
{
    printf "%s\n" ""
    printf "%s\n" "Generating fstab"
    local source_path="$1"
    local target_path="$2"
    genfstab -U "$source_path" >> "$target_path"
}

setup_root_for_system_config () {
    printf "%s\n" ""
    printf "%s\n" "Setting up root for system configuration"
    mkdir -p /mnt/root/install
    cp -r /root/install/* /mnt/root/install/
    ln -sf /mnt/root/install/do-system.sh /mnt/etc/profile.d/do-system.sh
}

end_message ()
{
    printf "%s\n" ""
    printf "%s\n" "The base install script is complete."
    printf "%s\n" "Before continuing you should change root to mnt,"
    printf "%s\n" "then execute the system configuration by executing the following commands:"
    printf "%s\n" "arch-chroot /mnt"
    printf "%s\n" "/root/install/3-system-config.sh"
    printf "%s\n" ""
}

pause ()
{
    printf "%s\n" ""
    local message="Press any key to continue . . ."
    read -r -t 10 -s -n 1 -p "$message" || true
    printf "%s\n" ""
}

main () {
    ### Setup script
    export DO_SYS_ENV_FILE="/root/install/do-system.sh"
    clear
    error_settings on
    start_message
    # Setup do system
    set_do_sys_env_var
    reset_base_install /mnt
    pause
    # 2.0 Installation
    # 2.1 select the mirrors
    change_pacman_settings
    update_mirrorlist
    # 2.2 install essential packages
    install_base_packages
    pause
    # 3.1 Fstab
    # This is created here because of flow in install scripts
    generate_fstab /mnt /mnt/etc/fstab
    pause
    ### Finalize script
    set_do_sys_env_var 1
    setup_root_for_system_config
    end_message
    error_settings off
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "### Script is being executed. ###\n"
    main
else
    printf " This script is being sourced.\n"
fi
